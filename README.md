# Amogus

> **OpenGL** based 2D maze game (inspired by *Among Us*).

Race against an impostor trying to kill you by finding your way out of a *perfect* maze while completing objectives to become the **radiant AMOGUS player**. And yes, it's a time-bound game.

## Building the game

Run the following from the project directory:

```bash
mkdir build/
cd build/
cmake ..
make
```

For running the game:
```bash
./amogus
```

## Game Specifics

- Character can't move b/w walls.
- Imposter is your **evil twin sister** wanting to kill you by stabbing slowly depleting your health.
  - She stabs the character 
- Impostor being evil-genius will search for you using *Breadth-First-Search* and *Backtracking* algorithm.
- Coins and Bombs after the player completes all the objectives to buy what he really wants - more game skins.

### Keys:

- `W`, `A`, `S`, `D`: Character movement
- `C`: Visibility across walls turned off.
- `V`: Visibility across walls turned on.
- `O`: Toggle world visibility.
  - `I` `J`, `K`, `L`: World movements
  - `M`, `N`: To zoom out/ in
- `P`: To turn off world camera view, i.e. to turn on first person view
- `ESC` or `Q`: To quit the application
